Special Menu Items Access
-------------------------

Module written by benjamin melançon of Agaric, http://agaric.com

Inspired by and using Special Menu Items module by Tamir Al Zoubi and Karim
Djelid of Servit Open Source Solutions, http://www.servit.ch

Description
-----------
Special Menu Items Access enables placeholder (text menu item which is not a
link) and separator (something like "-------") menu items used for groupings
and structure to follow the access control rules of regular menu items.

Special Menu Items - https://www.drupal.org/project/special_menu_items - is
needed for this module to have any useful effect, unless you write your own code
to do special handling of <nolink> or <separator> paths.

Features
--------

Contact
-------
This module is developed by Agaric, http://agaric.coop

It is inspired by and in part based on Special Menu Items by
Servit Open Source Solutions - http://servit.ch